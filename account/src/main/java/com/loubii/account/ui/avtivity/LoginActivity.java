package com.loubii.account.ui.avtivity;



import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.loubii.account.R;
import com.loubii.account.util.CountDownTimerUtils;
import com.loubii.account.util.DataUtil;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, View.OnKeyListener {

    private boolean hasSentCode;
    private EditText phone;
    private EditText code;
    private CheckBox accept;
    private Button codeLogin;
    private View otherLogin;
    private Button resend;
    private LinearLayout codeLayout;

    private CountDownTimerUtils mCountDownTimerUtils;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        code = (EditText) findViewById(R.id.edit_code);
        phone = (EditText) findViewById(R.id.edit_phone);
        accept = (CheckBox) findViewById(R.id.ck_accept);
        codeLogin = (Button) findViewById(R.id.btn_code_Login);
        otherLogin = findViewById(R.id.tv_other_login);
        resend = (Button) findViewById(R.id.btn_resend);
        codeLayout= (LinearLayout) findViewById(R.id.ll_code);


        resend.setOnClickListener(this);
        codeLogin.setOnClickListener(this);
        codeLogin.setEnabled(false);
        otherLogin.setOnClickListener(this);
        phone.setOnKeyListener(this);
        code.setOnKeyListener(this);
        mCountDownTimerUtils = new CountDownTimerUtils(resend, 5000, 1000);




    }

    @Override
    protected void onStart() {
        super.onStart();
        codeLayout.setVisibility(View.GONE);
        hasSentCode=false;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.btn_resend:
                sendCode();
                break;

            case R.id.btn_code_Login:
                if (hasSentCode){

                    if (!accept.isChecked()){
                        //提示需要同意
                        loginByPhone();
                        break;
                    }



                }else {

                    boolean phoneValid = DataUtil.isPhone(String.valueOf(phone.getText()));
                    Log.d("dd", "onClick: phoneValid:"+phoneValid);
                    if (!phoneValid){
                        //提示手机有误
                        break;
                    }

                    sendCode();
                    //切换到登录
                    hasSentCode=true;
                    codeLogin.setText("立即登录");
                    phone.setVisibility(View.GONE);
                    codeLayout.setVisibility(View.VISIBLE);

                }
                break;


        }

    }


    @Override
    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (!hasSentCode&& phone.getText().length()==11 || hasSentCode && code.getText().length()==4){
            codeLogin.setEnabled(true);
        }
        else {
            codeLogin.setEnabled(false);
        }
        return false;
    }
    private void sendCode(){
        //todo send code

        mCountDownTimerUtils.start();
        codeLogin.setEnabled(false);
    }
    private void loginByPhone(){
        //todo login

    }
}
