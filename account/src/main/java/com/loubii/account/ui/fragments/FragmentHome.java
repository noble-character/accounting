package com.loubii.account.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.loubii.account.R;
import com.loubii.account.adapter.BillAdapter;
import com.loubii.account.adapter.CenterAdapter;
import com.loubii.account.bean.AccountModel;
import com.loubii.account.constants.Config;
import com.loubii.account.constants.Extra;
import com.loubii.account.db.AccountModelDao;
import com.loubii.account.db.database.DBManager;
import com.loubii.account.db.database.DbHelper;
import com.loubii.account.ui.avtivity.CalendarActivity;
import com.loubii.account.util.TimeUtil;
import com.loubii.account.util.ToastUtil;
import com.marshalchen.ultimaterecyclerview.UltimateRecyclerView;
import com.marshalchen.ultimaterecyclerview.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;

/**
 * 首页
 */
public class FragmentHome extends BaseFragment {

    @BindView(R.id.home_page_today_outcome)
    TextView tvHomePageTodayOutcome;

    @BindView(R.id.home_page_today_income)
    TextView tvHomePageTodayIncome;

    @BindView(R.id.home_page_today_budget)
    TextView tvHomePageTodayBudget;

    @BindView(R.id.home_page_today_budget_percent)
    TextView tvHomePageTodayBudgetPercent;

    @BindView(R.id.home_page_month_outcome)
    TextView tvHomePageMonthOutcome;
    @BindView(R.id.home_page_recent_recycler_view)
    UltimateRecyclerView mUltimateRecyclerView;

    private static final String ARG_PARAM1 = "param1";
    private String mParam1;
    private DBManager<AccountModel, Long> mDbManager;
    private List<AccountModel> mAccountList = new ArrayList<>();
    private Date mCurrentDate;

    private BillAdapter mBillAdapter;
    private boolean mIsFirst = true;

    public FragmentHome() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDbManager = DbHelper.getInstance().author();
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    protected void initData() {
        initTitleData();
    }

    @Override
    protected void initView(View view) {
        initTodayView();
        initMonthView();
        initRecycleView();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_home;
    }


    private void initTitleData() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("MM月dd日");
        mCurrentDate = calendar.getTime();
        mAccountList.addAll(getAccountList(0, mCurrentDate));
    }

    private List<AccountModel> getAccountList(int offSet, Date currentDate) {
        List<AccountModel> accountList = mDbManager.queryBuilder()
                .where(AccountModelDao.Properties.Time.between
                        (TimeUtil.getFirstDayOfMonth(currentDate), TimeUtil.getEndDayOfMonth(currentDate)))
                .orderAsc(AccountModelDao.Properties.Time)
                .offset(offSet * Config.LIST_LOAD_NUM)
                .limit(Config.LIST_LOAD_NUM)
                .list();
        return accountList;
    }

    private void initTodayView() {
        double outcome = 0;
        double income = 0;
        double budget = 500;


        if (mAccountList.size() > 0) {
            for (AccountModel accountModel : mAccountList) {
                int type = accountModel.getOutIntype();
                if (type == 1) // 支出
                    outcome += accountModel.getCount();
                if (type == 2) // 收入
                    income += accountModel.getCount();
            }
        }

        tvHomePageTodayOutcome.setText(String.valueOf(outcome));
        tvHomePageTodayIncome.setText(String.valueOf(income));
        tvHomePageTodayBudget.setText(String.valueOf(budget));
        double percent = (budget - outcome) / budget * 100;
        String hint = "剩余";
        if (budget < outcome)
            hint = "超支";
        tvHomePageTodayBudgetPercent.setText("（" + hint + new DecimalFormat("#.00").format(percent) + "%）");
    }

    private void initMonthView() {
        double monthOutcome = 4356.32;
        this.tvHomePageMonthOutcome.setText(String.valueOf(monthOutcome));
    }

    private void initRecycleView() {
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        mUltimateRecyclerView.setLayoutManager(linearLayoutManager);
        mBillAdapter = new BillAdapter(mAccountList);
        mBillAdapter.setOnItemClickListener(new BillAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(context, CalendarActivity.class);
                intent.putExtra(Extra.ACCOUNT_DATE, mCurrentDate.getTime());
                getActivity().startActivity(intent);
                //ToastUtil.showShort(getActivity(), position + "");
            }
        });

        //悬浮头部布局需要加入
        StickyRecyclerHeadersDecoration headersDecor = new StickyRecyclerHeadersDecoration(mBillAdapter);
        mUltimateRecyclerView.addItemDecoration(headersDecor);
        //设置下拉刷新
        mUltimateRecyclerView.setDefaultSwipeToRefreshColorScheme(getResources().getColor(R.color.colorPrimary));
        mUltimateRecyclerView.setDefaultOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        changeList(0);
                        initTodayView();
                        ToastUtil.showShort("数据已更新");
                        //linearLayoutManager.scrollToPosition(0);
                    }
                }, 1000);
            }
        });
        mUltimateRecyclerView.setAdapter(mBillAdapter);
        mUltimateRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    //如果滑动到第一条且完全可见则展开appbarLayout
                    int visiblePosition = linearLayoutManager.findFirstCompletelyVisibleItemPosition();
                    if (visiblePosition == 0) {
//                        mAppBar.setExpanded(true);
                    }
                }
            }
        });
        mUltimateRecyclerView.setEmptyView(R.layout.rv_empty_bill, UltimateRecyclerView.EMPTY_CLEAR_ALL);
        if (mAccountList.size() == 0)
            mUltimateRecyclerView.showEmptyView();

        mIsFirst = false;
    }

    private void changeList(int monthDistance) {
        //当前月
        if (monthDistance > 0 && TimeUtil.date2String(new Date(), "yy年MM月")
                .equals(TimeUtil.date2String(mCurrentDate, "yy年MM月")))
            return;
        if (monthDistance != 0) {
            mCurrentDate = TimeUtil.getMonthAgo(mCurrentDate, monthDistance);
//            mTvTitleTime.setText(TimeUtil.date2String(mCurrentDate, "yy年MM月"));
        }
        List<AccountModel> accList = getAccountList(0, mCurrentDate);
        if (accList != null) {
            mAccountList.clear();
            mAccountList.addAll(accList);
            //mBillAdapter.onBindHeaderViewHolder();
            mBillAdapter.notifyDataSetChanged();
            if (accList.size() == 0) {
                if (mUltimateRecyclerView != null)
                    mUltimateRecyclerView.showEmptyView();
            } else {
                if (mUltimateRecyclerView != null)
                    mUltimateRecyclerView.hideEmptyView();
            }
        }
    }
}
